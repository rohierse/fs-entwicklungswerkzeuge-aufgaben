import unittest

# Gruppiere die Tests 1 und 4 in einer Testsuite und führe nur diese beiden Tests beim Skriptaufruf aus.

class SomeTests(unittest.TestCase):

    def test_one(self):
        self.assertEqual(1, 1)

    def test_two(self):
        self.assertEqual(2, 2)

    def test_three(self):
        self.assertEqual(3, 3)

    def test_four(self):
        self.assertEqual(4, 4)

    def test_five(self):
        self.assertEqual(5, 5)


if __name__ == '__main__':
