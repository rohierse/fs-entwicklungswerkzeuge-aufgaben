
# Funktion zur Überprüfung ob eine Zahl gerade ist (True/False)
def is_even_number(some_int):
    if some_int % 2 == 0:
        return True
    else:
        return False

def check_length(filename):
    with open(filename, "r") as f:
        return len(f.read())