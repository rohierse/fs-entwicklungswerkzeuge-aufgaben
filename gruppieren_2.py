import unittest

# Rufe das Skript über die Kommandozeile auf und führe dabei die Tests "SomeTests1: 1 und 4" sowie "SomeTests2: 8 und 10" aus.

class SomeTests1(unittest.TestCase):

    def test_one(self):
        self.assertEqual(1, 1)

    def test_two(self):
        self.assertEqual(2, 2)

    def test_three(self):
        self.assertEqual(3, 3)

    def test_four(self):
        self.assertEqual(4, 4)

    def test_five(self):
        self.assertEqual(5, 5)


class SomeTests2(unittest.TestCase):

    def test_six(self):
        self.assertEqual(6, 6)

    def test_seven(self):
        self.assertEqual(7, 7)

    def test_eight(self):
        self.assertEqual(8, 8)

    def test_nine(self):
        self.assertEqual(9, 9)

    def test_ten(self):
        self.assertEqual(10, 10)