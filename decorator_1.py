import unittest

# Test 3 ist falsch definiert und schlägt fehl, überspringe den Test mit einem decorator


class SomeTests(unittest.TestCase):

    def test_one(self):
        self.assertEqual(1, 1)

    def test_two(self):
        self.assertEqual(2, 2)

    def test_three(self):
        self.assertEqual(2, 3)


if __name__ == '__main__':
    unittest.main()